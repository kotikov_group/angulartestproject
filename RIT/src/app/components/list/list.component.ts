import {Component, OnInit} from '@angular/core';
import {PlanetsService} from "../../services/planets.service";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  pages: number[] = [];

  constructor(public swapiService: PlanetsService) {
  }

  ngOnInit() {
    this.swapiService.getAllPlanets().subscribe();
  }
}
