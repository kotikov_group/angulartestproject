import {Component, OnInit} from '@angular/core';
import {PlanetsService} from '../../services/planets.service';
import {IPlanet} from '../../models/planet';
import {ActivatedRoute} from '@angular/router';
import {ResidentService} from '../../services/resident.service';
import {IResident} from '../../models/resident';

@Component({
  selector: 'app-planet',
  templateUrl: './planet.component.html',
  styleUrls: ['./planet.component.scss']
})
export class PlanetComponent implements OnInit {
  planet: IPlanet | undefined;
  filteredResidents: IResident[] | null = null;

  constructor(
    public planetService: PlanetsService,
    private route: ActivatedRoute,
    public residentService: ResidentService
  ) {
  }

  ngOnInit() {
    this.planetService.getAllPlanets().subscribe(() => {
      this.route.params.subscribe((params) => {
        this.planet = this.planetService.getPlanetByName(params['name']);
        this.planet?.residents.forEach(resident => {
          this.residentService.getResident(resident).subscribe()
        });
      })
    });
  }

  selectGender(gender: string) {
    if (gender === 'reset') {
      this.filteredResidents = this.residentService.residents;
      return;
    }
    this.filteredResidents = this.residentService.getResidentsByGender(gender);
  }
}
