import {Component, Input} from '@angular/core';
import {IPlanet} from 'src/app/models/planet';

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss']
})
export class ListItemComponent {
  @Input() planet: IPlanet = {
    name: '',
    diameter: '',
    rotation_period: '',
    orbital_period: '',
    gravity: '',
    population: '',
    climate: '',
    terrain: '',
    surface_water: '',
    residents: [],
    films: [],
    url: '',
    created: '',
    edited: '',
  }
}

