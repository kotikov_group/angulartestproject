import {Component, Input} from '@angular/core';
import {PaginationService} from '../../services/pagination.service';
import {PlanetsService} from '../../services/planets.service';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent {
  constructor(public paginationService: PaginationService, private planetsService: PlanetsService) {}

  @Input() pages: number[] = [1];

  selectPage(index: number) {
    this.paginationService.setCurrentPage(index);
    this.planetsService.getAllPlanets().subscribe();
  }
}


