import {Injectable} from '@angular/core'
import {HttpClient} from '@angular/common/http'
import {IResident} from '../models/resident'
import {Observable, tap} from 'rxjs'

@Injectable({
  providedIn: 'root',
})
export class ResidentService {
  residents: IResident[] = [];

  constructor(private http: HttpClient) {
  }

  getResident(resident: string): Observable<IResident> {
    return this.http.get<IResident>(resident).pipe(tap(response => {
      this.residents.push(response);
    }));
  }

  getResidentsByGender(gender: string): IResident[] {
    return this.residents.filter(resident => resident.gender === gender);
  }
}
