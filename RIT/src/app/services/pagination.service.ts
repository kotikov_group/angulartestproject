import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class PaginationService {
  currentPage: number = 1;
  pages: number[] = [1];

  setCurrentPage(currentPage: number) {
    this.currentPage = currentPage;
  }

  setPages(pages: number[]) {
    this.pages = [...pages];
  }
}
