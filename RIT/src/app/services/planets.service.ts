import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable, tap} from 'rxjs';
import {IPlanet, IPlanetResponse} from '../models/planet';
import {PaginationService} from './pagination.service';

@Injectable({
  providedIn: 'root',
})
export class PlanetsService {
  planets: IPlanet[] = [];

  constructor(private http: HttpClient, private paginationService: PaginationService) {
  }

  getAllPlanets(): Observable<IPlanetResponse> {
    return this.http.get<IPlanetResponse>(`https://swapi.dev/api/planets`, {
      params: new HttpParams({
        fromObject: {page: this.paginationService.currentPage}
      }),
    }).pipe(tap((planetResponse) => {
      this.planets = planetResponse.results || [];
      if (planetResponse.results !== null) {
        this.paginationService.setPages(new Array(planetResponse.count / planetResponse.results.length).fill(1));
      }
    }));
  }

  getPlanetByName(name: string): IPlanet | undefined {
    return this.planets.find((planet) => planet.name === name);
  }
}
