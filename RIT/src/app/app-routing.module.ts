import {NgModule} from '@angular/core';
import {ListComponent} from './components/list/list.component';
import {RouterModule} from '@angular/router';
import {PlanetComponent} from './components/planet/planet.component';

const routes = [
  {
    path: '',
    component: ListComponent
  },
  {
    path: ':name',
    component: PlanetComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}
